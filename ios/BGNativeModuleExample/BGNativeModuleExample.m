//
//  BGNativeModuleExample.m
//  BGNativeModuleExample
//
//  Created by sfit0194 on 3/1/17.
//  Copyright © 2017 sfit0194. All rights reserved.
//

#import "BGNativeModuleExample.h"

#import <React/RCTLog.h>

@implementation BGNativeModuleExample

RCT_EXPORT_MODULE(BGNativeModuleExample);

RCT_EXPORT_METHOD(testPrint:(NSString *)name info:(NSDictionary *)info) {
      RCTLogInfo(@"%@: %@", name, info);
}

@end
