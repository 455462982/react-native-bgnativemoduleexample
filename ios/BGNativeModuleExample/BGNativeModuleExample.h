//
//  BGNativeModuleExample.h
//  BGNativeModuleExample
//
//  Created by sfit0194 on 3/1/17.
//  Copyright © 2017 sfit0194. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridge.h>

@interface BGNativeModuleExample : NSObject<RCTBridgeModule>

@end
